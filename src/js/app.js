const my_news = [
    {
        id: 1,
        author: 'Alexander Owenss',
        text: 'Lorem ipsum...',
        bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж'
    },
    {
        id: 2,
        author: 'Just Vasya',
        text: 'The $ should cost 35!',
        bigText: 'А евро 42!'
    },
    {
        id: 3,
        author: 'Guest',
        text: 'Free...',
        bigText: 'А евро опять выше 70.'
    }
];

class Article extends React.Component {
    state = {
        visible: false,
    };

    handleReadMoreClick = (e) => {
        e.preventDefault();
        this.setState({visible: true})
    };

    render() {
        const {author, text, bigText} = this.props.data;
        const {visible} = this.state;
        return (
            <div className="article">
                <p className="news__author">{author}:</p>
                <p className="news__text">{text}</p>
                {
                    !visible && <a onClick={this.handleReadMoreClick} href="#" className="news__readmore">Details</a>
                }
                {
                    visible && <p className="news__big-text">{bigText}</p>
                }
            </div>
        )
    }
}

Article.propTypes = {
    data: PropTypes.shape({
        author: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        bigText: PropTypes.string.isRequired
    })
}

class News extends React.Component {
    state = {
        counter: 0
    };

    handleCounter = () => {
        this.setState({counter: ++this.state.counter})
    };

    renderNews() {
        const {data} = this.props;
        let newsTemplate = null;

        if (data.length) {
            newsTemplate = data.map(function (item) {
                return <Article key={item.id} data={item}/>
            });
        } else {
            newsTemplate = <p>Sorry, no news</p>
        }
        return newsTemplate
    };

    render() {
        const {data} = this.props;
        const {counter} = this.state;
        return (
            <div className="news">
                {this.renderNews()}
                {
                    data.length ? <strong onClick={this.handleCounter} className={'news__count'}>News
                        count: {data.length}</strong> : null
                }
                <p>Total clicks: {counter}</p>
            </div>
        );
    }
}

const Comments = () => {
    return (
        <div className="comments">
            No news - no comments!
        </div>
    );
};

class Add extends React.Component {
    state = {
        name: 'Your name',
        text: 'Issue text',
        accepted: false,
    };

    onNameChange = (e) => {
        this.setState({name: e.currentTarget.value})
    };

    onTextChange = (e) => {
        this.setState({text: e.currentTarget.value})
    };

    onAccept = (e) => {
        this.setState({accepted: e.currentTarget.checked})
    };

    onBtnClickHandler = (e) => {
        e.preventDefault();
        const {name, text} = this.state
        alert(name + '\n' + text)
    };

    render() {
        const {name, text, accepted} = this.state;
        return (
            <form className='add'>
                <input
                    type='text'
                    className='add__author'
                    placeholder='Your name'
                    onChange={this.onNameChange}
                    value={name}/>
                <textarea className='add__text'
                          placeholder='Issue text'
                          onChange={this.onTextChange}
                          value={text}/>
                <label className='add__checkrule'>
                    <input type='checkbox' onChange={this.onAccept}/> I accept rules
                </label>
                <button className='add__btn'
                        onClick={this.onBtnClickHandler}
                        disabled={!accepted}>
                    Показать alert
                </button>
            </form>
        )
    }
}

const App = () => {
    return (
        <div className="app">
            <Add/>
            <h3>News</h3>
            <News data={my_news}/>
            <Comments/>
        </div>
    );
};

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);